package Zadanie1LiczbySlownie;

import java.util.List;

public class Million {
	public static List<String> millions(int x, String[] slowa, String[] setki, List<String> list, String[] dziesiatki,
			String[] tysiace, String[] miliony) {
		int absX = Math.abs(x);
		if (absX >= 1000000) {

			int mil = absX / 1000000;

			if (mil == 1) {
				Unit.jednosci(mil, slowa, list);
				list.add(miliony[0]);
			} else if (mil >= 2 && mil <= 4) {
				Unit.jednosci(mil, slowa, list);
				list.add(miliony[1]);
			} else if (mil < 20) {
				Unit.jednosci(mil, slowa, list);
				list.add(miliony[2]);
			} else if (mil < 100) {
				Dozens.dziesiatki(mil, slowa, dziesiatki, list);
				int konc = mil % 10;
				if (konc >= 2 && konc <= 4)
					list.add(miliony[1]);
				else
					list.add(miliony[2]);
			} else {
				if (mil % 10 >= 2 && mil % 10 <= 4) {
					Hundreds.setki(mil, slowa, setki, list, dziesiatki);
					list.add(miliony[1]);
				} else {
					Hundreds.setki(mil, slowa, setki, list, dziesiatki);
					list.add(miliony[2]);
				}
			}
			absX = absX - mil * 1000000;
			Thousand.thousands(absX, slowa, setki, list, dziesiatki, tysiace);
			Hundreds.setki(absX, slowa, setki, list, dziesiatki);
			Dozens.dziesiatki(absX, slowa, dziesiatki, list);
			Unit.jednosci(absX, slowa, list);
		}

		return list;
	}
}
