package Zadanie1LiczbySlownie;

import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Zadanie1 {
/*
	public static List<String> jednosci(int x, String[] slowa, List<String> list) {
		int absX = Math.abs(x);

		if (x == 0)
			list.add(slowa[Math.abs(absX)]);
		if (x < 20 && x > 0)
			list.add(slowa[Math.abs(absX)]);

		return list;
	}

	public static List<String> dziesiatki(int x, String[] slowa, String[] dziesiatki, List<String> list) {

		int absX = Math.abs(x);
		String s = absX + "";

		if (absX > 19 && absX < 100) {

			int y = s.charAt(0) - 48;

			String konc = "";

			if (y == 2)
				konc = dziesiatki[1];
			else if (y == 3)
				konc = dziesiatki[2];
			else if (y > 4)
				konc = dziesiatki[3];
			else if (y == 0)
				konc = "";

			if (y == 4) {
				list.add("czterdzie�ci ");
			} else if (!slowa[y].equals("zero")) {
				list.add(slowa[y]);
			}
			list.add(konc);

			absX = absX - y * 10;
			jednosci(absX, slowa, list);
		}

		return list;
	}

	public static List<String> setki(int x, String[] slowa, String[] setki, List<String> list, String[] dziesiatki) {
		int absX = Math.abs(x);
		String s = absX + "";
		int p = s.charAt(0) - 48;
		if (absX >= 100 && absX < 1000) {

			if (absX < 200)
				list.add("sto ");
			else if (absX >= 200 && absX < 300)
				list.add("dwie�cie ");
			else if (absX >= 300) {
				list.add(slowa[p]);
				if (p <= 4)
					list.add(setki[2]);
				else
					list.add(setki[3]);
			}
			absX = absX - p * 100;
			dziesiatki(absX, slowa, dziesiatki, list);
			jednosci(absX, slowa, list);

		}
		return list;
	}

	public static List<String> thousands(int x, String[] slowa, String[] setki, List<String> list, String[] dziesiatki,
			String[] tysiace) {
		int absX = Math.abs(x);

		if (absX >= 1000) {

			int tous = absX / 1000;

			if (tous == 1) {
				jednosci(tous, slowa, list);
				list.add(tysiace[0]);
			} else if (tous >= 2 && tous <= 4) {
				jednosci(tous, slowa, list);
				list.add(tysiace[1]);
			} else if (tous < 20) {
				jednosci(tous, slowa, list);
				list.add(tysiace[2]);
			} else if (tous < 100) {
				dziesiatki(tous, slowa, dziesiatki, list);
				int konc = tous % 10;
				if (konc >= 2 && konc <= 4)
					list.add(tysiace[1]);
				else
					list.add(tysiace[2]);
			} else {
				if (tous % 10 >= 2 && tous % 10 <= 4) {
					setki(tous, slowa, setki, list, dziesiatki);
					list.add(tysiace[1]);
				} else {
					setki(tous, slowa, setki, list, dziesiatki);
					list.add(tysiace[2]);
				}
			}
			absX = absX - tous * 1000;
			setki(absX, slowa, setki, list, dziesiatki);
			dziesiatki(absX, slowa, dziesiatki, list);
			jednosci(absX, slowa, list);
		}

		return list;
	}

	public static List<String> millions(int x, String[] slowa, String[] setki, List<String> list, String[] dziesiatki,
			String[] tysiace, String[] miliony) {
		int absX = Math.abs(x);
		if (absX >= 1000000) {

			int mil = absX / 1000000;

			if (mil == 1) {
				jednosci(mil, slowa, list);
				list.add(miliony[0]);
			} else if (mil >= 2 && mil <= 4) {
				jednosci(mil, slowa, list);
				list.add(miliony[1]);
			} else if (mil < 20) {
				jednosci(mil, slowa, list);
				list.add(miliony[2]);
			} else if (mil < 100) {
				dziesiatki(mil, slowa, dziesiatki, list);
				int konc = mil % 10;
				if (konc >= 2 && konc <= 4)
					list.add(miliony[1]);
				else
					list.add(miliony[2]);
			} else {
				if (mil % 10 >= 2 && mil % 10 <= 4) {
					setki(mil, slowa, setki, list, dziesiatki);
					list.add(miliony[1]);
				} else {
					setki(mil, slowa, setki, list, dziesiatki);
					list.add(miliony[2]);
				}
			}
			absX = absX - mil * 1000000;
			thousands(absX, slowa, setki, list, dziesiatki, tysiace);
			setki(absX, slowa, setki, list, dziesiatki);
			dziesiatki(absX, slowa, dziesiatki, list);
			jednosci(absX, slowa, list);
		}

		return list;
	}*/

	public static void write(int x) {

		String[] slowa = { "", "jeden", "dwa", "trzy", "cztery", "pi��", "sze��", "siedem", "osiem", "dziewi��",
				"dziesi��", "jedena�cie", "dwana�cie", "trzyna�cie", "czterna�cie", "pi�tna�cie", "szesna�cie",
				"siedemna�cie", "osiemna�cie", "dziewi�tna�cie" };

		String[] dziesiatki = { "na�cie ", "dzie�cia ", "dzie�ci ", "dziesi�t " };
		String[] setki = { "sto ", "dwie�cie ", "sta ", "set " };
		String[] tysiace = { " tysi�c ", " tysi�ce ", " tysi�cy " };
		String[] miliony = { " milion ", " miliony ", " milion�w " };

		List<String> list = new LinkedList<>();
		int absX = Math.abs(x);

		String minus = "minus ";

		if (x == 0)
			list.add("zero");

		if (x < 0)
			list.add(minus);

		if (absX < 20)
			Unit.jednosci(absX, slowa, list);
		else if (absX < 100)
			Dozens.dziesiatki(absX, slowa, dziesiatki, list);
		else if (absX < 1000)
			Hundreds.setki(absX, slowa, setki, list, dziesiatki);
		else if (absX < 1000000)
			Thousand.thousands(absX, slowa, setki, list, dziesiatki, tysiace);
		else
			Million.millions(absX, slowa, setki, list, dziesiatki, tysiace, miliony);
		;

		Iterator<String> iter = list.iterator();
		while (iter.hasNext())
			System.out.print(iter.next());

	}

	public static void main(String[] args) {
		System.out.println("Podaj liczb�");
		Scanner scaner = new Scanner(System.in);
		try {
	//		@SuppressWarnings("deprecation")
			int x = scaner.nextInt();
			//int x = new Integer("25260");
			

			write(x);
			
		} catch (NumberFormatException e) {
			System.out.println("b��dne dane");
		}
		catch (InputMismatchException e) {
			System.out.println("b��dne dane");
		}
		finally {
			scaner.close();
		}

	}

}
