package Zadanie1LiczbySlownie;

import java.util.List;

public class Dozens {
	
	public static List<String> dziesiatki(int x, String[] slowa, String[] dziesiatki, List<String> list) {

		int absX = Math.abs(x);
		String s = absX + "";

		if (absX > 19 && absX < 100) {

			int y = s.charAt(0) - 48;

			String konc = "";

			if (y == 2)
				konc = dziesiatki[1];
			else if (y == 3)
				konc = dziesiatki[2];
			else if (y > 4)
				konc = dziesiatki[3];
			else if (y == 0)
				konc = "";

			if (y == 4) {
				list.add("czterdzieści ");
			} else if (!slowa[y].equals("zero")) {
				list.add(slowa[y]);
			}
			list.add(konc);

			absX = absX - y * 10;
			Unit.jednosci(absX, slowa, list);
		}

		return list;
	}
}
