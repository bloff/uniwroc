package Zadanie1LiczbySlownie;

import java.util.List;

public class Unit {
	
	public static List<String> jednosci(int x, String[] slowa, List<String> list) {
		int absX = Math.abs(x);

		if (x == 0)
			list.add(slowa[Math.abs(absX)]);
		if (x < 20 && x > 0)
			list.add(slowa[Math.abs(absX)]);

		return list;
	}
	
	public int suma (int a, int b) {
		return a + b;
	}
}
