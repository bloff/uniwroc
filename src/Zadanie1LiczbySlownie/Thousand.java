package Zadanie1LiczbySlownie;

import java.util.List;

public class Thousand {
	public static List<String> thousands(int x, String[] slowa, String[] setki, List<String> list, String[] dziesiatki,
			String[] tysiace) {
		int absX = Math.abs(x);

		if (absX >= 1000) {

			int tous = absX / 1000;

			if (tous == 1) {
				Unit.jednosci(tous, slowa, list);
				list.add(tysiace[0]);
			} else if (tous >= 2 && tous <= 4) {
				Unit.jednosci(tous, slowa, list);
				list.add(tysiace[1]);
			} else if (tous < 20) {
				Unit.jednosci(tous, slowa, list);
				list.add(tysiace[2]);
			} else if (tous < 100) {
				Dozens.dziesiatki(tous, slowa, dziesiatki, list);
				int konc = tous % 10;
				if (konc >= 2 && konc <= 4)
					list.add(tysiace[1]);
				else
					list.add(tysiace[2]);
			} else {
				if (tous % 10 >= 2 && tous % 10 <= 4) {
					Hundreds.setki(tous, slowa, setki, list, dziesiatki);
					list.add(tysiace[1]);
				} else {
					Hundreds.setki(tous, slowa, setki, list, dziesiatki);
					list.add(tysiace[2]);
				}
			}
			absX = absX - tous * 1000;
			Hundreds.setki(absX, slowa, setki, list, dziesiatki);
			Dozens.dziesiatki(absX, slowa, dziesiatki, list);
			Unit.jednosci(absX, slowa, list);
		}

		return list;
	}
}
