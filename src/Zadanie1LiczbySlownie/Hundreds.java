package Zadanie1LiczbySlownie;

import java.util.List;

public class Hundreds {
	public static List<String> setki(int x, String[] slowa, String[] setki, List<String> list, String[] dziesiatki) {
		int absX = Math.abs(x);
		String s = absX + "";
		int p = s.charAt(0) - 48;
		if (absX >= 100 && absX < 1000) {

			if (absX < 200)
				list.add("sto ");
			else if (absX >= 200 && absX < 300)
				list.add("dwie�cie ");
			else if (absX >= 300) {
				list.add(slowa[p]);
				if (p <= 4)
					list.add(setki[2]);
				else
					list.add(setki[3]);
			}
			absX = absX - p * 100;
			Dozens.dziesiatki(absX, slowa, dziesiatki, list);
			Unit.jednosci(absX, slowa, list);

		}
		return list;
	}
}
